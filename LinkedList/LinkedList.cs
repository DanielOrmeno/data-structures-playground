using System;
namespace LinkedListDS {
    public class LinkedList: IDataStructure
    {
        private Node head {get; set;}
        public Node Head => head;

        public void Clear()
        {
            head = null;
        }
        public bool Exists(int val)
        {
            return false;
        }
        public int PositionOf(int val)
        {
            var count = 0;
            var tmp = head;

            while (tmp != null)
            {
                if (tmp.Value == val)
                    return count;
                tmp = tmp.Next;
                count++;
            }
            return -1; //- Value does not exist;
        }
        public void InsertAtTail(int val)
        {
            if (head == null)
            {
                head = new Node(val);
            }
            else
            {
                var tmp = head;
                while (tmp.Next != null)
                {
                    tmp = tmp.Next;
                }
                tmp.Next = new Node(val);
            }
        }

        public void InsertAtHead(int val)
        {
            if (head == null)
            {
                head = new Node(val);
            }
            else 
            {
                var tmp = new Node(val);
                tmp.Next = head;
                head = tmp;
            }
        }

        public void InsertAtPosition(int val, int position)
        {
            if (position < 0)
            {
                Console.WriteLine("Invalid Position, must be greater than 0");
                return;
            }
            if (position == 0)
            {
                InsertAtHead(val);
                return;
            }
            var tmp = head;
            var count = 0;

            while (tmp != null)
            {
                if (count+1 == position)
                {
                    var t2 = new Node(val);
                    t2.Next = tmp.Next;
                    tmp.Next = t2;
                    return;
                }
                tmp = tmp.Next;
                count++;
            }
            
            Console.WriteLine("Could not add Node, position is out of bounts");
        }
        public void RemoveWithValue(int val)
        {
            var tmp = head;

            if (head == null)
            {
                Console.WriteLine("Cannot remove node from an empty list");
                return;
            }
            if (tmp.Value == val)
            {
                head = tmp.Next;
                return;
            }

            while (tmp.Next != null)
            {
                if (tmp.Next.Value == val)
                {
                    tmp.Next = tmp.Next.Next;
                    return;
                }
                tmp = tmp.Next;
            }

            Console.WriteLine($"Could not delete node, value {val} not found in list");
        }
        public void RemoveAtPosition(int position)
        {
            var count = 0;
            var tmp = head;

            if (position < 0)
            {
                Console.WriteLine("Could not remove node as position is out of bounds");
                return;
            }
            if (head == null)
            {
                Console.WriteLine("Cannot remove node from an empty list");
                return;
            }
            if (position == 0 && tmp != null)
            {
                head = tmp.Next;
                return;
            }

            while (tmp != null)
            {
                if (count + 1 == position)
                {
                    var t2 = tmp.Next;
                    tmp.Next = t2.Next;
                    return;
                }
                tmp = tmp.Next;
                count ++;
            }

            Console.WriteLine("Could not delete node, positions is out of bounds");
        }

        public void Invert()
        {
            Node newHead = null;
            var tmp = head;

            if (tmp == null)
                return;
            
            while (tmp != null)
            {
                var t2 = new Node(tmp.Value);
                t2.Next = newHead;
                newHead = t2;
                tmp = tmp.Next;
            }

            head = newHead;
        }

        public void Print()
        {
            var tmp = head;
            var list = string.Empty;
            
            while (tmp != null)
            {
                list = list.Equals(string.Empty) ? tmp.Value.ToString() : string.Concat(list,$" -> {tmp.Value}");
                tmp = tmp.Next;
            }

            Console.WriteLine(list);
        }
    }
}