using System;

namespace BinaryTreeDS
{
    public class BinaryTree: IDataStructure
    {
        private Node head {get; set;}
        public Node Head => head;

        public void Insert(int val)
        {
            if (head == null)
            {
                var n = new Node(val);
                head = n;
            }
            else
            {
                InsertRecursive(head, val);   
            }
        }

        public void Remove(int val)
        {
            if (head == null)
            {
                Console.WriteLine("Tree is currently Empty");
            }
            else
            {
                RemoveRecursive(head, val);
            }
        }

        public void Print()
        {
            if (head != null)
            {
                PrintRecursive(head);
            }
            else 
            {
                Console.WriteLine("Tree is currently Empty");
            }

        }

        public void PrintNode(Node node)
        {
            var left = node.Left?.Value.ToString() ?? "Empty";
            var right = node.Right?.Value.ToString() ?? "Empty";
            Console.WriteLine($"Node {node.Value}\n|->L: {left}\n|->R: {right}\n");
        }

        public Node FindValue(int val)
        {
            return FindValueRecursive(head, val);
        }

        public void Clear()
        {
            head = null;
        }

        //- Private Members
        private void PrintRecursive(Node node)
        {
            if (node == null)
                return;
            
            PrintNode(node);

            if (node.Left != null)
            {
                PrintRecursive(node.Left);
            }
            if (node.Right != null)
            {
                PrintRecursive(node.Right);
            }
        }

        private Node InsertRecursive(Node node, int val)
        {
            if (val < node.Value && node.Left == null)
            {
                node.Left = new Node(val);
                return node;
            }
            else if (val > node.Value && node.Right == null) 
            {
                node.Right = new Node(val);
                return node;
            }
            else if (val == node.Value)
            {
                Console.WriteLine($"Could not insert Node with value {val}, value already exists");
                return node;
            }
            else 
            {
                return InsertRecursive( val < node.Value ? node.Left : node.Right, val);
            }
        }

        private Node FindValueRecursive(Node node, int val, int depth = 0)
        {
            if (val == node.Value)
            {
                Console.WriteLine($"Value {val} found at depth {depth}");
                return node;
            }
            else if (val < node.Value && node.Left != null)
            {
                depth++;
                return FindValueRecursive(node.Left, val, depth);
            }
            else if (node.Right != null)
            {
                depth++;
                return FindValueRecursive(node.Right, val, depth);
            }

            return null; //- Not Found;
        }

        private void RemoveRecursive(Node node, int val)
        {
            if (node == null)
                return;

            if (node.Left?.Value == val)
            {
                node.Left = PerformRemoval(node.Left);
            }
            else if (node.Right?.Value == val)
            {
                node.Right = PerformRemoval(node.Right);
            }
            else 
            {
                RemoveRecursive( val < node.Value ? node.Left : node.Right, val);
            }
        }

        private Node PerformRemoval(Node node)
        {
            //- Case 1 - Has two childs
            if (node.Left != null && node.Right != null)
            {
                while (node.Left != null)
                    node = node.Left; //- get to lowerest left leaf
                return node;
            }
            //- Case 2 - Only one child
            else if (node.Left != null)
            {
                return node.Left;
            }
            else if (node.Right != null)
            {
                return node.Right;
            }
            //- Case 3 - No childs
            else
            {
                return null;
            }
        }
    }
}