using System;
using StackArrayDS;
public class SAMasterCommander : IMasterCommander
{
    public bool ShouldExit{get; set;}
    private StackArray stack;

    public SAMasterCommander()
    {
        stack = new StackArray();
    }

    public void Execute()
    {
        while (!ShouldExit)
        {
            PrintOptions();
            ResolveInput(Console.ReadLine().ToLower());
        }
    }

    public void PrintOptions()
    {
        Console.WriteLine("\n");
        Console.WriteLine("Please enter a command:");
        Console.WriteLine("A.- Generate Random Stack (Clears Previous DS)");
        Console.WriteLine("B.- Pop");
        Console.WriteLine("C.- Push");
        Console.WriteLine("D.- Peek");
        Console.WriteLine("E.- Print Stack");
        Console.WriteLine("Z.- Return to main menu");
    }

    public void ResolveInput(string input)
    {
        switch (input)
        {
            case "a":
            stack.Clear();
            generateRandomStack();
            break;
            case "b":
            stack.Pop();
            stack.Print();
            break;
            case "c":
            pushValue();
            stack.Print();
            break;
            case "d":
            peekLast();
            break;
            case "e":
            stack.Print();
            break;
            case "z":
            ShouldExit = true;
            break;
            default:
            Console.WriteLine("Unrecognized Option");
            break;
        }
    }

    private void peekLast()
    {
        stack.Peek();
    }

    private void pushValue()
    {
        var n = ReadNumber("Insert a value to push");
        stack.Push(n);
    }

    private void generateRandomStack()
    {
        var random = new Random();
        var n = ReadNumber("Enter the number of items to be inserted into the stack");
        Console.WriteLine($"Generating a new tree of {n} random nodes");
        int i = 0;
        while (i<n)
        {
            stack.Push(random.Next(-n, n));
            i++;
        }
        stack.Print();
    }

    private int ReadNumber(string message)
    {
        Console.WriteLine(message);
        while (true)
        {
            try
            {
                var num = Convert.ToInt32(Console.ReadLine());
                return num;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Invalid Input, please specify a number");
            }
        }
    }
}