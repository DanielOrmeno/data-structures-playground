using System;
namespace StackArrayDS 
{
    public class StackArray : IDataStructure
    {
        public StackArray()
        {
            top = -1;
            stack = new int[100];
        }
        private int[] stack;
        private int top;

        public void Clear()
        {
            while (top != -1)
            {
                top--;
            }
        }

        public void Print()
        {
            for (var i = top; i >= 0; i--)
                Console.WriteLine(stack[i]);
        }

        public void Peek()
        {
            if (top != -1)
                Console.WriteLine($"Value {stack[top]} is at the top of the stack");
        }
        public void Pop()
        {
            if (top != -1)
            {
                resize();

                var val = stack[top--];
                Console.WriteLine($"Popped value {val}");
            }
        }

        public void Push(int val)
        {
            if (top != -1)
                resize();
            stack[++top] = val;
        }

        private void resize()
        {
            if (top == stack.Length-1)
            {
                var newStack = new int[stack.Length*2];
                for (var i = 0; i < stack.Length; i++)
                {
                    newStack[i] = stack[i];
                }
                stack = newStack;
            }
            else if (top <= stack.Length/4)
            {
                var newStack = new int[stack.Length/2];
                for (var i = 0; i < newStack.Length; i++)
                {
                    newStack[i] = stack[i];
                }
            }
        }
    }
}