public interface IMasterCommander 
{
    bool ShouldExit {get; set;}
    void Execute();
    void PrintOptions();
    void ResolveInput(string input);
}