# Data Structures Implementation in C# #

Simple implementation of Data Structures in C#

## Linked List ##

### Node Template: ###

```
#!c#

class Node {
    int value;
    Node Next;
}
```

### Operations: ###

* Generate Random List (Clears Previous DS)
* Insert Node at Head
* Insert Node at Tail
* Insert Node at N
* Remove Node at N
* Remove Node with value
* Invert List
* Print List
* Find Position of value

## Binary Tree ##

### Node Template: ###

```
#!c#

class Node {
    int value;
    Node Left;
    Node Right;
}
```

### Operations: ###

* Generate Random Tree (Clears Previous DS)
* Find node with value
* Insert Node
* Remove Node
* Print Tree

## Stack ##

### Node Template: ###

```
#!c#

class Node {
    int value;
    Node Next;
}
```

### Operations: ###

* Generate Random Stack (Clears Previous DS)
* Pop
* Push
* Peek
* Print Stack

## =D ##